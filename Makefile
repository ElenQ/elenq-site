LANGS              := es en eu
CONFIG             := ./babel.cfg

TEMPLATEDIR        := ./templates
TEMPLATES          := $(wildcard $(TEMPLATEDIR)/*.html)

EXTRACT            := ./locale/messages.pot
LOCALEDIR          := $(dir $(EXTRACT))
LOCALESOURCES      := $(addsuffix /LC_MESSAGES/messages.po, $(addprefix $(LOCALEDIR)/, $(LANGS)))

STATIC             := ./static

SITEOUTPUT         := ./www

.DEFAULT_GOAL:= site

$(EXTRACT): $(TEMPLATES)
	@echo "Extracting locale file..."
	@mkdir -p $(LOCALEDIR)
	@pybabel extract -F $(CONFIG) -o $(EXTRACT) $(TEMPLATEDIR)
	@echo "Extracted!"

init_translation_files: $(EXTRACT)
	@echo "Creating translation file templates..."
	@for L in $(LANGS); do \
		pybabel init -l $$L -d $(LOCALEDIR) -i $(EXTRACT) ;\
	done
	@echo "Created!"

compile_translation: $(LOCALESOURCES)
	@echo "Compiling locales..."
	@pybabel compile -f -d $(LOCALEDIR)
	@echo "Compiled!"

update_translation_files: $(EXTRACT)
	@echo "Updating translation files..."
	@for L in $(LANGS); do \
		pybabel update -l $$L -d $(LOCALEDIR) -i $(EXTRACT) ;\
	done
	@echo "Updated!"

site: compile_translation $(TEMPLATES)
	@echo "Creating site in $(SITEOUTPUT)..."
	@echo "Creating HTML..."
	@./create.py -l $(LANGS) -t $(TEMPLATEDIR) --localedir $(LOCALEDIR) -o $(SITEOUTPUT)
	@echo "Created!"
	@echo "Copying static files..."
	@cp --recursive $(STATIC) $(SITEOUTPUT)
	@echo "Copied!"
	@echo "Site created!"

clean:
	@echo "Cleaning..."
	@rm -rf $(SITEOUTPUT)/* $(LOCALEDIR)/*/*/*.mo
	@echo "Clean!"


.PHONY: init_translation_files compile_translation update_translation_files site clean
