# DEPRECATED: Moved to [HERE](https://gitlab.com/ElenQ/site)

All the site will be static and with just the necessary JavaScript.

No external JavaScript libraries if possible. No big CSS/JS frameworks like
Bootstrap.

It runs with Skeleton-css and custom CSS.

The site will be translated to Spanish (es), English (en) and Euskera (eu).
Repository contains also the translation files.

The site and the contents are not made yet. The site creator just works.


# Multilanguage Static Site generator

> NOTE:
>
> It's not intended to be a good piece of software, just works for ElenQ Website.
> Maybe in the future it will be. Who knows.


This website creator is based on Jinja2 and Babel to create translatable HTML
templates and render them ordered by language in a separate folder.

# File structure

All the Jinja2 templating functionality is written in python in the `create.py`
script.

The rest of the translation functionality is written in the Makefile at the
moment, that's why it's not the best solution of the world, but works.

All the Jinja2 templates must be stored in `templates` folder, if any template
is not intended to be rendered (because it's just a base template for
inheritance or something like that) it's name must start with `_` or `.`.

A `static` folder contains all the static files like JavaScript, CSS or images.

> NOTE:
>
> If the scripts create some output, it would need to be translated also.
> Put that part inside the template with the corresponding `gettext()` or `_()`
> functions.
>
> `static` folder is not translated.

At the moment, all the locations are hardcoded in the Makefile but are
configurable in all the steps of the process. Overwrite the Makefile if you
need to change them.

# Normal usage

- Create the templates using Jinja2 in the templates folder using `gettext` or
  `_` in all translatable strings.
- Put all the static stuff under `static` folder arranged as you like.
- Call `make init_translation_files` to create translation templates.
- Fill the translations.
- Call `make compile_translation` to compile the translation.
- Call `make site` to create the whole site under `www` folder. (It will
  compile the translations also)

If after the translation, any template is changed, translation files can be
reconstructed without loosing the translation job done before. Run `make
update_translation_files` for that.

# Note about templates

All the Jinja2 templates are rendered with `lang` and `all_langs` variables
set.
- `lang` variable stores the language of the current template. As the whole
  site will be replicated under this language it's recommended to use this
  variable as prefix in all the hyperlinks to the translated version like this:
  ``` html
  <a href='/{{ lang }}/contact.html'>{{ _(Contact) }}</a>
  ```
  This way all the links will point to the same language of the current page.

- `all_langs` variable stores all the languages the site is translated to. It
  is useful to create links to all the languages like this:
  ``` html
  {% for l in all_langs %}
    <a href='{{ l }}/index.html'><img src='/static/img/flag_{{ l }}.jpg'/></a>
  {% endfor %}
  ```

# Improvements

TODO list to make this more useful and cool:

- Remove that Makefile and automate it better
- Translate also JavaScript? This will replicate all the JavaScript...
    - Is it enough now? -> Put modules in `static` and user output in
      `templates`. Only translate the user output.
- Make the translation thing better
    - Be careful with Jinja2 integration.
    - ~~Move to Node.js/CoffeeScript?~~ MOVED TO NODE.JS. Check
      [schiumato](https://gitlab.com/ElenQ/schiumato)
