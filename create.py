#!/usr/bin/env python

from jinja2 import Environment, FileSystemLoader, select_autoescape
import argparse
from os import path, listdir, makedirs
from babel.support import Translations


def getTemplates( directory ):
    """
    Returns all the names on the templates directory to be rendered.
    Just discards the files starting with . or _
    """
    return list(filter(lambda x: not (x.startswith('_') or x.startswith('.')), listdir(directory)))

if __name__ == '__main__':

    # Parse input arguments:
    parser = argparse.ArgumentParser(description='Creat the static site ElenQ style')
    parser.add_argument('--language', '-l', default=['es','en','eu'], nargs='+',
        help='Select the languages to translate to. Use multiple times.')
    parser.add_argument('--outdir', '-o', default='www',
        help='Select output directory to render templates in.')
    parser.add_argument('--templatedir', '-t', default='templates',
        help='Select template directory.')
    parser.add_argument('--localedir', default='locale',
        help='''Select translation directory this must be the structure:
                localedir/lang/LC_MESSAGES/message.mo''')
    args = parser.parse_args()

    # Prepare template loader
    autoescape = select_autoescape(['html', 'xml'])
    i18n = ['jinja2.ext.i18n']
    loader = FileSystemLoader(args.templatedir, encoding = 'utf-8', followlinks=True)

    if not path.isdir(args.templatedir):
        raise IOError('File not found: ' + args.templatedir)
    if not path.isdir(args.localedir):
        raise IOError('File not found: ' + args.localedir)
    if not path.isdir(path.dirname(path.abspath(args.outdir))):
        raise IOError('File not found: ' + path.dirname(path.abspath(args.outdir)))

    # Get templates to render
    templates = getTemplates(args.templatedir)

    # Create output directory if doesn't exist
    if not path.isdir(args.outdir):
        makedirs(args.outdir)

    # Create an environment per translation and render the templates
    for lang in args.language:
        # Make dir for this language
        langpath = path.join(args.outdir, lang)
        if not path.isdir(langpath):
            makedirs(langpath)

        # Create translation
        #translation = gettext.translation('messages', languages=[lang], localedir=args.localedir)
        translation = Translations.load(args.localedir, [lang])
        env = Environment(loader=loader, autoescape=autoescape, extensions = i18n )
        env.install_gettext_translations(translation, newstyle=True)

        # Render all the templates
        for template in templates:
            output = env.get_template(template).render(lang=lang, all_langs=args.language)
            where = path.join(langpath, template)
            with open(where, 'w') as f:
                f.write(output)
